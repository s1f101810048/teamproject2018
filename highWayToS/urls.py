"""highWayToS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from iniadRead import views as userViews
from secure import views as secureViews
#from . import views

urlpatterns = [
    # User Authencication
    path(r'inout/', include('django.contrib.auth.urls')), #add Auth
    path(r'secure/create/', secureViews.create, name='register'),


    # Function
    path(r'about/', userViews.about),
    path(r'', userViews.index),

    # Book Controller
    path(r'book/all/<int:page>/', userViews.showAllBooks, name='allBooks'),
    path(r'book/add/', userViews.addBook, name='addBook'),
]

#Document
#   inout/login/ [name='login']
#   inout/logout/ [name='logout']
