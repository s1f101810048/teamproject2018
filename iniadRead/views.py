from django.shortcuts import render, redirect
from django.http import Http404
from django.utils import timezone
from django.http import JsonResponse as jsonResponse

from django.contrib.auth.models import User
from .models import Book, Comment

def index(request):
    return render(request, 'iniadRead/index.html')

def about(request):
    return render(request, 'iniadRead/about.html')

#Books Controller
def addBook(request):
    if request.method == 'POST':
        book = Book(
            title=request.POST['title'], 
            intro=request.POST['intro'], 
            image_url=request.POST['image_url'], 
            posted_at=timezone.now(),
            status=1,
            user_id=request.user
        )
        book.save()
        return jsonResponse(request.POST)
    else:
        return render(request, 'booksCtrl/add.html')

def showAllBooks(request, page):
    books = Book.objects.order_by('posted_at')
    context = {
        'books': books
    }
    return render(request, 'booksCtrl/showAll.html', context)