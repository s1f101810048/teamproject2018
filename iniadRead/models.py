from django.db import models
from django.contrib.auth.models import User

class Book(models.Model):
    title = models.CharField(max_length=200)
    intro = models.TextField()
    image_url = models.TextField()
    posted_at = models.DateTimeField('date published')
    user_id = models.ForeignKey(User, related_name="books", on_delete=models.CASCADE, default=0)
    status = models.IntegerField(default=0)

class Comment(models.Model):
    text = models.TextField()
    posted_at = models.DateTimeField('date published')
    book_id = models.ForeignKey(Book, related_name="comments", on_delete=models.CASCADE, default=0)
    user_id = models.ForeignKey(User, related_name="comments", on_delete=models.CASCADE, default=0)
# Create your models here.
